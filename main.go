package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"reflect"
	"text/tabwriter"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

func main() {

	input := os.Args[1:]
	token := ""
	if len(input) > 0 {
		token = input[0]
	} else {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter Token: ")
		token, _ = reader.ReadString('\n')
	}

	var mc jwt.MapClaims

	_, _ = jwt.ParseWithClaims(token, &mc, func(token *jwt.Token) (interface{}, error) {
		return token, nil
	})
	// initialize tabwriter
	w := new(tabwriter.Writer)

	// minwidth, tabwidth, padding, padchar, flags
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)

	fmt.Print("\n")
	fmt.Print("Standard Claims (RFC7519)\n")
	fmt.Print("-----------------------------\n")
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "JWT ID", "(jti):", mc["jit"])
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "Subject ID", "(sub):", mc["sub"])
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "Expiration Date", "(exp):", time.Unix(int64(math.Round(mc["exp"].(float64))), 0))
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "Issued Date", "(iat):", time.Unix(int64(math.Round(mc["iat"].(float64))), 0))
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "Not Before", "(nbf):", time.Unix(int64(math.Round(mc["nbf"].(float64))), 0))
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "Issuer", "(iss):", mc["iss"])
	fmt.Fprintf(w, "%s\t%s\t%v\t\n", "Audience", "(aud):", mc["aud"])

	w.Flush()

	// initialize tabwriter
	w2 := new(tabwriter.Writer)

	// minwidth, tabwidth, padding, padchar, flags
	w2.Init(os.Stdout, 8, 8, 0, '\t', 0)

	fmt.Print("\n")
	fmt.Println("Non-standard Claims")
	fmt.Print("-----------------------------\n")

	for k, v := range mc {
		if k != "jti" &&
			k != "sub" &&
			k != "exp" &&
			k != "iat" &&
			k != "nbf" &&
			k != "iss" &&
			k != "aud" {
			rt := reflect.TypeOf(v)
			switch rt.Kind() {
			case reflect.Slice:
				fallthrough
			case reflect.Array:
				fmt.Fprintf(w2, "%s\t\t[%s\t\n", k, "")
				for _, vs := range v.([]interface{}) {
					fmt.Fprintf(w2, "%s\t\t  %s\t\n", "", vs)
				}
				fmt.Fprintf(w2, "%s\t\t]%s\t\n", "", "")
			default:
				fmt.Fprintf(w2, "%s\t\t: %s\t\n", k, v)
			}
		}
	}
	w2.Flush()
}
